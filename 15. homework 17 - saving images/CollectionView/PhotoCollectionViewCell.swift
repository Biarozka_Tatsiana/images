

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func configure (with object: Photo) {
        let image = UIImage(named: object.imageName)
        imageView.image = image
        imageView.contentMode = .scaleToFill
    }
}
