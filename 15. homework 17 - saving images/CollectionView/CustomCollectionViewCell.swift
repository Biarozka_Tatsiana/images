

import UIKit

protocol CustomCollectionViewCellDelegate: AnyObject {
    
    func createAlert()
}

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var addingButton: UIButton!
    
    weak var delegate: CustomCollectionViewCellDelegate?

    func configure() {
        
        addingButton.setImage(UIImage(systemName: "plus"), for: .normal)
    }

    @IBAction func addingButtonPressed(_ sender: Any) {
        
        self.delegate?.createAlert()
    }
}
