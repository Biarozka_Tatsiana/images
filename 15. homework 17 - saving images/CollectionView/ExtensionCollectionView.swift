

import Foundation
import UIKit

extension SecondViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photos.count + stepIndex
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == indexFirst {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell
            else {
                return UICollectionViewCell()
            }
            cell.configure()
            cell.delegate = self
            return cell
        } else {
            guard let cellPhoto = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as? PhotoCollectionViewCell else {
                return UICollectionViewCell()
            }
            
            cellPhoto.configure(with: photos[indexPath.item - stepIndex])
            let loadedImage = loadImage(fileName: photos[index].imageName)
            if  loadedImage != nil {
                cellPhoto.imageView.image = loadedImage
            }
            
            if index < self.photos.count - stepIndex {
                index += stepIndex
            } else {
                index = indexFirst
            }
            return cellPhoto
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row >= stepIndex {
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController else {return}
            controller.indexPhoto = indexPath.row - stepIndex
            controller.isSuccess = isSuccess
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side: CGFloat = (view.frame.width - 10)/2
        return CGSize(width: side, height: side)
    }
}

extension SecondViewController: CustomCollectionViewCellDelegate {
    func createImagePiker () {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func createAlert () {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let  cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.createImagePiker()
        }
        let photoLibraryAction = UIAlertAction(title: "Library", style: .default) { (_) in
            self.createImagePiker()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cameraAction)
        alert.addAction(photoLibraryAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
}


