

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var photos: [Photo] = []
    let rowSectionFirstIndex = 1
    let row = 0
    var index = 0
    var indexFirst = 0
    let stepIndex = 1
    var isSuccess = true
    let cellAmountInRow: CGFloat = 2
    let distanveBetweenCells: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fakePhotos = UserDefaults.standard.value([Photo].self, forKey: Keys.fakeKey.rawValue)
        if fakePhotos == nil || fakePhotos?.isEmpty == true {
            UserDefaults.standard.set(encodable: [Photo(imageName: "Coastline", favorite: false, comment: ""),
            Photo(imageName: "Horizon", favorite: false, comment: ""),
            Photo(imageName: "Rocks", favorite: false, comment: ""),
            Photo(imageName: "Shoreline", favorite: false, comment: ""),
            Photo(imageName: "Silhouette", favorite: false, comment: "")], forKey: Keys.fakeKey.rawValue)
        }
        let key = isSuccess ? Keys.successKey.rawValue : Keys.fakeKey.rawValue
        guard let photos = UserDefaults.standard.value([Photo].self, forKey: key) else { return }
        self.photos = photos
    }
}

extension SecondViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            if let imageName = saveImage(pickedImage: pickedImage) {
                let photoModel = Photo(imageName: imageName, favorite: false, comment: "")
                photos.insert(photoModel, at: indexFirst)
                collectionView.reloadData()
                UserDefaults.standard.set(encodable: photos,
                                          forKey: isSuccess ? Keys.successKey.rawValue : Keys.fakeKey.rawValue)
            }
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

