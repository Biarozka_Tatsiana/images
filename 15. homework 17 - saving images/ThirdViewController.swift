

import UIKit

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!    
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var topImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomImageConstraint: NSLayoutConstraint!
    
    var photos: [Photo] = []
    let imagesNextView = UIImageView()
    var indexPhoto = 0
    var isSelected = false
    let animationTimeInterval = 0.5
    let delay = 0
    var tapConstraint = 0
    let startIndex = 0
    let stepIndex = 1
    var imageZoomed = false
    let topImage: CGFloat = 70
    let bottomImage: CGFloat = 400
    let leadingImage: CGFloat = 30
    let trailingImage: CGFloat = 30
    var isSuccess = true
    let keyboardCoordinate: CGFloat = 0.0
    
    enum DirectionMove {
        case right
        case left
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let photos = UserDefaults.standard.value([Photo].self, forKey: isSuccess ? Keys.successKey.rawValue : Keys.fakeKey.rawValue) else { return }
        self.photos = photos
        let startImage = loadImage(fileName: photos[self.indexPhoto].imageName)
        if startImage != nil {
            imageView.image = startImage
        } else {
            imageView.image = UIImage(named: photos[self.indexPhoto].imageName)
        }
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        commentTextField.text = photos[self.indexPhoto].comment
        isSelected = photos[self.indexPhoto].favorite
        updateLikeButton()
        commentTextField.delegate = self
        
        let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(processSwipe(_:)))
        rightSwipeGesture.direction = .right
        view.addGestureRecognizer(rightSwipeGesture)
        
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(processSwipe(_:)))
        leftSwipeGesture.direction = .left
        view.addGestureRecognizer(leftSwipeGesture)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(_:)))
        imageView.addGestureRecognizer(recognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ThirdViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ThirdViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: keyboardCoordinate, left: keyboardCoordinate, bottom: keyboardSize.height , right: keyboardCoordinate)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func keyboardWillHide(notification: NSNotification) {
        
        let contentInsets = UIEdgeInsets(top: keyboardCoordinate, left: keyboardCoordinate, bottom: keyboardCoordinate, right: keyboardCoordinate)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func processSwipe(_ swipeGesture: UISwipeGestureRecognizer) {
        
        switch swipeGesture.direction {
        case.right:
            addImagesRight()
        case.left:
            removeImagesLeft ()
        default: break
        }
    }
    
    @IBAction func tapDetected(_ recognizer: UITapGestureRecognizer) {
        
        if !self.imageZoomed {
            UIView.animate(withDuration: animationTimeInterval, delay: TimeInterval(delay), options: .curveLinear) {
                self.scaleImageUp()
                self.imageZoomed = true
            }
        } else {
            UIView.animate(withDuration: animationTimeInterval, delay: TimeInterval(delay), options: .curveLinear) {
                self.normalizeImage()
                self.imageZoomed = false
            } completion: { (_) in
                self.commentTextField.isHidden = false
                self.likeButton.isHidden = false
            }
        }
    }
    
    func scaleImageUp () {
        
        self.topImageConstraint.constant = CGFloat(self.tapConstraint)
        self.bottomImageConstraint.constant = CGFloat(self.tapConstraint)
        self.leadingImageConstraint.constant = CGFloat(self.tapConstraint)
        self.trailingImageConstraint.constant = CGFloat(self.tapConstraint)
        view.layoutIfNeeded()
        self.commentTextField.isHidden = true
        self.likeButton.isHidden = true
    }
    
    func normalizeImage() {
        
        self.topImageConstraint.constant = topImage
        self.bottomImageConstraint.constant = bottomImage
        self.leadingImageConstraint.constant = leadingImage
        self.trailingImageConstraint.constant = trailingImage
        view.layoutIfNeeded()
    }
    
    func addImagesRight () {
        
        if self.indexPhoto < self.photos.count - stepIndex {
            self.indexPhoto += stepIndex
        } else {
            self.indexPhoto = startIndex
        }
        imagesNextView.frame = imageView.frame
        self.imagesNextView.frame.origin.x = -self.imageView.frame.maxX
        self.setImageNext()
        self.scrollView.addSubview(imagesNextView)
        UIView.animate(withDuration: animationTimeInterval, delay: 0, options: .curveLinear) {
            
            self.imagesNextView.frame.origin.x = self.imageView.frame.origin.x
            
        } completion: { (_) in
            
            self.setImageCurrent()
            self.imagesNextView.removeFromSuperview()
            self.isSelected = self.photos[self.indexPhoto].favorite
            self.updateLikeButton()
            self.commentTextField.text = self.photos[self.indexPhoto].comment
        }
    }
    
    func removeImagesLeft () {
        
        self.scrollView.addSubview(imagesNextView)
        self.setImageNext()
        if self.indexPhoto == startIndex {
            let current = self.loadImage(fileName: self.photos.last!.imageName)
            if current != nil {
                self.imageView.image = current
            } else {
                self.imageView.image = UIImage(named: self.photos.last!.imageName)
            }
        } else {
            let current = self.loadImage(fileName: self.photos[self.indexPhoto - stepIndex].imageName)
            if current != nil {
                self.imageView.image = current
            } else {
                self.imageView.image = UIImage(named: self.photos[self.indexPhoto - stepIndex].imageName)
            }
        }
        self.imagesNextView.frame = self.imageView.frame
        UIView.animate(withDuration: animationTimeInterval, delay: TimeInterval(delay), options: .curveLinear) {
            
            self.imagesNextView.frame.origin.x = -self.imageView.frame.maxX
            
        } completion: { (_) in
            self.imagesNextView.removeFromSuperview()
            self.isSelected = self.photos[self.indexPhoto].favorite
            self.updateLikeButton()
            self.commentTextField.text = self.photos[self.indexPhoto].comment
        }
        
        if indexPhoto > startIndex {
            self.indexPhoto -= stepIndex
        } else {
            self.indexPhoto = photos.count - stepIndex
        }
    }
    
    func setImageCurrent() {
        
        let current = self.loadImage(fileName: self.photos[self.indexPhoto].imageName)
        if current != nil {
            self.imageView.image = current
        } else {
            self.imageView.image = UIImage(named: self.photos[self.indexPhoto].imageName)
        }
    }
    
    func setImageNext() {
        
        let next = loadImage(fileName: photos[self.indexPhoto].imageName)
        if next != nil {
            imagesNextView.image = next
        } else {
            imagesNextView.image = UIImage(named: photos[self.indexPhoto].imageName)
        }
    }
    
    func saveComment () {
        
        guard let comment = commentTextField.text else {return}
        photos[self.indexPhoto].comment = comment
        UserDefaults.standard.set(encodable: photos, forKey: isSuccess ? Keys.successKey.rawValue : Keys.fakeKey.rawValue)
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        
        isSelected = !isSelected
        updateLikeButton()
    }
    
    func updateLikeButton () {
        
        if isSelected {
            photos[self.indexPhoto].favorite = true
            likeButton.tintColor = .red
        } else {
            photos[self.indexPhoto].favorite = false
            likeButton.tintColor = .gray
        }
        UserDefaults.standard.set(encodable: photos, forKey: isSuccess ? Keys.successKey.rawValue : Keys.fakeKey.rawValue)
    }
} 

extension ThirdViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.saveComment()
        return true
    }
}
