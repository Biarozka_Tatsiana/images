

import UIKit
import SwiftyKeychainKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let radius = 15
    let keychain = Keychain(service: "tatiana.-5--homework-17---saving-images")
    let correctPasswordKey = KeychainKey<String>(key: "correctPassword")
    let incorrectPasswordKey = KeychainKey<String>(key: "incorrectPassword")
    let successPassword = "12345"
    let fakePassword = "54321"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainImageView.image = UIImage(named: "Reflection 4")
        mainImageView.contentMode = .scaleToFill
        blurView.layer.cornerRadius = CGFloat(radius)
        saveCorrectPasswordKeychain()
        saveIncorrectPasswordKeychain()
    }
    
    func saveCorrectPasswordKeychain() {
        
        do {
            try keychain.set(successPassword, for : correctPasswordKey)
        } catch let error {
            debugPrint(error)
        }
    }
    
    func saveIncorrectPasswordKeychain() {
        do {
            try keychain.set(fakePassword, for : incorrectPasswordKey)
        } catch let error {
            debugPrint(error)
        }
    }
    
    func createWarningAlert() {
        
        let warningAlert = UIAlertController(title: "WARNING", message: "Your password is incorrect", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Re-enter", style: .destructive) { (_) in
        }
        warningAlert.addAction(okAction)
        present(warningAlert, animated: true)
    }
    
    func createTransitionSecond () {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else {return}
        if passwordTextField.text == getCorrectPasswordValue() {
            controller.isSuccess = true
        } else {
            controller.isSuccess = false
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func getCorrectPasswordValue() -> String? {
        do {
            let valueCorrect = try keychain.get(correctPasswordKey)
            return valueCorrect
        } catch let error {
            debugPrint(error)
            return nil
        }
    }
    
    func getIncorrectPasswordValue() -> String? {
        do {
            let valueIncorrect = try keychain.get(incorrectPasswordKey)
            return valueIncorrect
        } catch let error {
            debugPrint(error)
            return nil
        }
    }
    
    func checkPassword () {
        
        if passwordTextField.text == getCorrectPasswordValue() || passwordTextField.text == getIncorrectPasswordValue() {
            createTransitionSecond()
        } else {
            createWarningAlert()
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        checkPassword()
        return true
    }
}

