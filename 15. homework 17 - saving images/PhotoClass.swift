

import Foundation

class Photo: Codable {
    
    var imageName: String
    var favorite: Bool
    var comment: String
    
    init (imageName: String, favorite: Bool, comment: String) {
        self.imageName = imageName
        self.favorite = favorite
        self.comment = comment
    }
    private enum CodingKeys: String, CodingKey {
        case imageName
        case favorite
        case comment
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        imageName = try container.decode(String.self, forKey: .imageName)
        favorite = try container.decode(Bool.self, forKey: .favorite)
        comment = try container.decode(String.self, forKey: .comment)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.imageName, forKey: .imageName)
        try container.encode(self.favorite, forKey: .favorite)
        try container.encode(self.comment, forKey: .comment)
    }
}



